import edu.cest.lab01.Pessoa;
import edu.cest.lab01.Cliente;
import edu.cest.lab01.Animal;
public class Aplicacao {
	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa();
		pessoa1.nome = "Anderson";
		Cliente cliente1 = new Cliente();
		cliente1.nome = "Diego";
		Animal animal1 = new Animal();
		animal1.nome = "Dudu";
		
		System.out.println("Nome da pessoa: " + pessoa1.nome);				System.out.println("Nome do cliente: " + cliente1.nome);
		System.out.println("Nome do animal: " + animal1.nome);
}
}
